import { nanoid } from 'nanoid';

// HEAD DATA
export const headData = {
  title: 'Josh Scharlach - Skater Boi', // e.g: 'Name | Developer'
  lang: 'en', // e.g: en, es, fr, jp
  description: 'The newest sensation on the board.  Josh Scharlach, skate boarder exrrodinaire.  Full of videos, clips and his personal favs.', // e.g: Welcome to my website
};

// HERO DATA
export const heroData = {
  img: 'jtran3.png',
  title: '',
  name: '',
  subtitle: '',
  cta: '',
};

// ABOUT DATA
export const aboutData = {
  img: 'profile.jpg',
  paragraphOne: '',
  paragraphTwo: '',
  paragraphThree: '',
  resume: 'https://www.resumemaker.online/es.php', // if no resume, the button will not show up
};

// PROJECTS DATA
export const projectsData = [
  {
    id: nanoid(),
    img: 'vidjosh.mp4',
    title: '',
    info: '',
    info2: '',
    url: '',
    repo: 'https://github.com/cobidev/react-simplefolio',
  },
  // {
  //   id: nanoid(),
  //   img: 'project.jpg',
  //   title: '',
  //   info: '',
  //   info2: '',
  //   url: '',
  //   repo: 'https://github.com/cobidev/react-simplefolio',
  // },
  // {
  //   id: nanoid(),
  //   img: 'project.jpg',
  //   title: '',
  //   info: '',
  //   info2: '',
  //   url: '',
  //   repo: 'https://github.com/cobidev/react-simplefolio', 
  // },
];

// CONTACT DATA
export const contactData = {
  cta: '',
  btn: '',
  email: '',
};

// FOOTER DATA
export const footerData = {
  networks: [
    // {
    //   id: nanoid(),
    //   name: 'twitter',
    //   url: '',
    // },
    // {
    //   id: nanoid(),
    //   name: 'codepen',
    //   url: '',
    // },
    {
      id: nanoid(),
      name: 'linkedin',
      url: 'https://www.linkedin.com/in/joel-scharlach-626515174/',
    },
    {
      id: nanoid(),
      name: 'github',
      url: 'https://gitlab.com/J_scharlach',
    },
  ],
};
  export const favoriteData = {
    networks: [
      {
        id: nanoid(),
        cat: "Shops",
        name: 'Twits',
        img: "twits.png",
        url: 'http://www.twits.be/',
      },
      {
        id: nanoid(),
        cat: "Decks",
        name: 'Baker',
        img: "baker.png",
        url: 'https://bakerskateboards.com/',
      },
      {
        id: nanoid(),
        cat: "Wheels",
        name: 'Spitfire',
        img: "twits.png",
        url: 'https://www.spitfirewheels.com/',
      },
      {
        id: nanoid(),
        cat: "Trucks",
        name: 'Tensor',
        img: "trucks.png",
        url: 'https://tensortrucks.com/',
      },
      {
        id: nanoid(),
        cat: "Bearings",
        name: 'Bones',
        img: "bearings.png",
        url: 'https://bonesbearings.com/',
      },
    ],
};
