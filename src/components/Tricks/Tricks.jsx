import React from 'react';
import { Container } from 'react-bootstrap';


const Tricks = () => {

  return (
    <section id="tricks">
      <Container>
        <div className="tricks-sl">
            <span>kickflip</span>
            <span>ollie</span>
            <span>50-50 grind</span>
            <span>noseslide</span>
            <span>tailslide</span>
        </div>
      </Container>
    </section>
  );
};

export default Tricks;