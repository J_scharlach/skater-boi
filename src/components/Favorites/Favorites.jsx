import React, { useContext } from 'react';
import Fade from 'react-reveal/Fade';
import { Container, Row, Col } from 'react-bootstrap';
import PortfolioContext from '../../context/context';
import Title from '../Title/Title';
import Bearings from '../Image/bbearings';
import Deck from '../Image/bdeck';
import Shop from '../Image/bshop';
import Trucks from '../Image/btrucks';
import Wheels from '../Image/bwheels';

const Favorite = () => {
  const { favorite } = useContext(PortfolioContext);
  const { cat, name, img, url } = favorite;

  return (
    <section id="favorite">
      <Container>
        <div className="titledebomb title">
          <Title title="The Bomb"/>
        </div>
        <Fade bottom duration={1000} delay={800} distance="30px">
        <div className="divbombcombine">
          <div className="divbomb">
            <Row className="d-flex justify-content-center">
              <Col xs={12} lg={4} className="div1">
                <Shop alt="Shop picture"/>
              </Col>
              <Col xs={12} lg={4} className="div2">
                <Bearings alt="Bearings picture"/>
              </Col>
              <Col xs={12} lg={4} className="div3">
                <Deck alt="Deck picture"/>
              </Col>
            </Row>
          </div> 
          <div className="divbomb2">  
            <Row> 
              <Col xs={12} lg={6} className="div4">
                <Trucks alt="Trucks picture"/>
              </Col>
              <Col xs={12} lg={6} className="div5">
                <Wheels alt="Wheels picture"/>
              </Col>
            </Row>
          </div> 
        </div> 
        </Fade>
      </Container>
    </section>
  );
};

export default Favorite;