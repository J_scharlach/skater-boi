import React, { useState, useEffect } from 'react';
import Hero from './Hero/Hero';
import About from './About/About';
import Projects from './Projects/Projects';
import Favorite from './Favorites/Favorites';
import Success from './Success/Success';
import Contact from './Contact/Contact';
import Footer from './Footer/Footer';

import { PortfolioProvider } from '../context/context';

import { heroData, aboutData, projectsData, contactData, footerData, favoriteData } from '../mock/data';

function App() {
  const [hero, setHero] = useState({});
  const [about, setAbout] = useState({});
  const [projects, setProjects] = useState([]);
  const [contact, setContact] = useState({});
  const [footer, setFooter] = useState({});
  const [favorite, setFavorite] = useState({});

  useEffect(() => {
    setHero({ ...heroData });
    setAbout({ ...aboutData });
    setProjects([...projectsData]);
    setContact({ ...contactData });
    setFooter({ ...footerData });
    setFavorite({ ...favoriteData });
  }, []);

  return (
    <PortfolioProvider value={{ hero, about, projects, contact, footer, favorite}}>
      <Hero />
      <About />
      <Projects />
      <Success />
      <Favorite />
      <Contact />
      <Footer />
    </PortfolioProvider>
  );
}

export default App;
