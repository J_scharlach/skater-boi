import React from 'react';
import { withPrefix } from 'gatsby';

const Video = ({ src }) => {
    return (
        <video
            autoPlay
            muted
            loop
            playsInline
            style={{ height: 'auto', width: '50%', objectFit: 'fill', objectPosition: 'center', borderRadius: '20px' }}
            src={withPrefix(src)}
        >
            <source src={withPrefix(src)} type="video/mp4" />
            Your device does not support playing 'video/mp4' videos
        </video>
    )
}

export default Video