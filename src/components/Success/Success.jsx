import React from 'react';
import Fade from 'react-reveal/Fade';
import { Container} from 'react-bootstrap';
import Fall1 from '../Image/fall1';
import Fall2 from '../Image/fall2';
import Fall3 from '../Image/fall3';

const Success = () => {

  return (
    <section id="success">
      <Container className="project-wrapper">
        <Fade bottom duration={1000} delay={800} distance="30px">
          <div className="picdiv">
            <Fall1 className="logotop tt"/>
          </div>
          <h2 className="successlabels">Fail</h2>
          <div className="picdiv">
            <Fall2 className="logobottom tt"/>
          </div>
          <h2 className="successlabels">Fail</h2>
          <div className="picdiv">
            <Fall3 />
          </div>
          <h2 className="successlabels">Success feels great</h2>
        </Fade>
      </Container>
    </section>
  );
};

export default Success;