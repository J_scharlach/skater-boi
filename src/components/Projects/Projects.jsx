import React, { useContext, useEffect, useState } from 'react';
import Fade from 'react-reveal/Fade';
import { Container, Row, Col } from 'react-bootstrap';
import PortfolioContext from '../../context/context';
import Title from '../Title/Title';
import Video from '../Video/video';
// import { graphql } from "gatsby"
// import { GatsbyImage, getImage } from "gatsby-plugin-image"

const Projects = () => {
  const { projects } = useContext(PortfolioContext);

  const [isDesktop, setIsDesktop] = useState(false);
  const [isMobile, setIsMobile] = useState(false);

  // const image = getImage(data.boardImg.childImageSharp.fluid)

  useEffect(() => {
    if (window.innerWidth > 769) {
      setIsDesktop(true);
      setIsMobile(false);
    } else {
      setIsMobile(true);
      setIsDesktop(false);
    }
  }, []);

  return (
    <section id="projects">
      <Container>
        <div className="project-wrapper">
          <Title title="TRICKS" />
          {projects.map((project) => {
            const { title, info, info2, id } = project;
            return (
              <Row>
                <Col lg={4} sm={12} className="infodiv">
                  <Fade left={isDesktop} bottom={isMobile}duration={1000} delay={500}distance="30px">
                    <div className="project-wrapper__text">
                      <h3 className="project-wrapper__text-title">{title || 'Just a trick'}</h3>
                      <div>
                        <p>
                          {info ||
                            'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Excepturi neque, ipsa animi maiores repellendu distinctioaperiam earum..'}
                        </p>
                      </div>
                    </div>
                  </Fade>
                </Col>
                <Col lg={8} sm={12}>
                  <Fade right={isDesktop} bottom={isMobile} duration={1000}delay={1000}distance="30px">
                    <div className="project-wrapper__image">
                    <div>
                      <Video src='/vidjosh.mp4' />
                    </div>
                    </div>
                  </Fade>
                </Col>
              <Col lg={4} sm={12} className="infodiv betweenvids">
                <Fade left={isDesktop}bottom={isMobile}duration={1000}delay={500}distance="30px">
                  <div className="project-wrapper__text">
                    <h3 className="project-wrapper__text-title">{title || 'My first trick'}</h3>
                    <div>
                      <p>
                        {info ||
                          'Lorem ipsum dolor sit. Excepturi neque, ipsa animi maiores repellendu distinctioaperiam'}
                      </p>
                    </div>
                  </div>
                </Fade>
              </Col>
              <Col lg={8} sm={12} className="betweenvids">
                <Fade right={isDesktop} bottom={isMobile}duration={1000} delay={1000}distance="30px">
                  <div className="project-wrapper__image">
                  <div>
                    <Video src='/vidjosh2.mp4' />
                  </div>
                  </div>
                </Fade>
              </Col>
            </Row>
            );
          })}
        </div>
      </Container>
      <Fade left duration={1500} delay={500} exit={true}>
        <div>
          <img src='/sboard.png' alt="small skate board" className="smallboard"/>
        </div>
      </Fade>
      <Fade left duration={1500} delay={1000} exit={true}>
        <img src='/sboard.png' alt="small skate board" className="smallboard"/>
        <hr className="hrline"></hr>
      </Fade>
    </section>
  );
};

// export const pageQuery = graphql`
//   query {
//     boardImg: file(relativePath: {eq: "bearings.png"}) {
//       childImageSharp {
//         fluid {
//           ...GatsbyImageSharpFluid
//         }
//       }
//    }
//  }
// `  

export default Projects;




