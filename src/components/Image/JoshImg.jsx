import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import styles from './JoshImg.module.css'

export default function logonav () {
    return (
      <StaticQuery
        query={graphql`
        query JoshImg {
          file(relativePath: {eq: "jtran3.png"}) {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
        `}
        render={data => (
          <div >
            <Img className={styles.dataimg} fluid={data.file.childImageSharp.fluid} />
          </div>
        )}
      />
    )
  }