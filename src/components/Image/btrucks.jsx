import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import styles from './b.module.css'

export default function logonav () {
    return (
      <StaticQuery
        query={graphql`
        query truckImg {
          file(relativePath: {eq: "trucks.png"}) {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
        `}
        render={data => (
          <div >
            <Img className={styles.dataimg} fluid={data.file.childImageSharp.fluid} />
          </div>
        )}
      />
    )
  }