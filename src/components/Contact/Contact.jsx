import React, { useContext } from 'react';
import Fade from 'react-reveal/Fade';
import { Container } from 'react-bootstrap';
import PortfolioContext from '../../context/context';
import Title from '../Title/Title';
import { FaInstagram } from "react-icons/fa";
import { FaWhatsapp } from "react-icons/fa";


const Contact = () => {
  const { contact } = useContext(PortfolioContext);
  const { cta, btn, email } = contact;

  return (
    <section id="contact">
      <Container>
        <Title title="Contact" />
        <Fade bottom duration={1000} delay={800} distance="30px">
          <div className="contact-wrapper">
            <p className="contact-wrapper__text">
              {cta || 'Would you like to work with me? Awesome!'}
            </p>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href={email ? `mailto:${email}` : 'https://www.instagram.com/josh.ccapalot/'}
            >
              <FaInstagram className="insta"/>
            </a>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href='https://wa.me/0032479172881/'
            >
              <FaWhatsapp className="insta"/>
            </a>
          </div>
        </Fade>
      </Container>
    </section>
  );
};

export default Contact;
