import React, { useContext, useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Fade from 'react-reveal/Fade';
import JoshImg from '../Image/JoshImg';
import PortfolioContext from '../../context/context';
import Tricks from '../../components/Tricks/Tricks';
import { FaArrowDown } from "react-icons/fa";

const Header = () => {
  const { hero } = useContext(PortfolioContext);
  const { title, subtitle } = hero;

  const [isDesktop, setIsDesktop] = useState(false);
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    if (window.innerWidth > 769) {
      setIsDesktop(true);
      setIsMobile(false);
    } else {
      setIsMobile(true);
      setIsDesktop(false);
    }
  }, []);

  return (
    <section id="hero" className="jumbotron">
      <Container>
        <Fade left={isDesktop} bottom={isMobile} duration={1000} delay={500} distance="30px">
          <h1 className="hero-title">
            {title || 'Joshua Thomas Scharlach'}{' '}
          </h1>  
            <br />
            <h1 className="hero-subtitle">  
            {subtitle || "Skater Boi"}
          </h1>
        </Fade>
        <Row>
          <Col xs={3}>
            <Tricks />
          </Col> 
          <Col xs={9}>
            <Fade right={isDesktop} bottom={isMobile} duration={5000} delay={1000} distance="130px">
              <JoshImg alt="profile picture"/>
            </Fade>
          </Col> 
        </Row>
        <div className="scroll">
          <FaArrowDown />
        </div>
      </Container>
    </section>
  );
};

export default Header;
