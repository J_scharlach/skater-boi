import React, { useContext, useState, useEffect } from 'react';
import Fade from 'react-reveal/Fade';
import Zoom from 'react-reveal/Zoom';
import { Container, Row, Col } from 'react-bootstrap';
import Title from '../Title/Title';
import PortfolioContext from '../../context/context';
import Profile from '../Image/Profile';
import Tilt from 'react-tilt'

const About = () => {
  const { about } = useContext(PortfolioContext);
  const { paragraphOne, paragraphTwo, paragraphThree } = about;

  const [isDesktop, setIsDesktop] = useState(false);
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    if (window.innerWidth > 769) {
      setIsDesktop(true);
      setIsMobile(false);
    } else {
      setIsMobile(true);
      setIsDesktop(false);
    }
  }, []);

  return (
    <section id="about">
      <Container>
        <Title title="Just your average" />
        <Title title="American/ Canadian/ English/ Belgian kid" />
        <Row className="about-wrapper">
          <Col md={6} sm={12}>
            {/* <Fade bottom duration={2000} delay={600} distance="30px"> */}
            <Zoom>
                <Tilt className="Tilt profilepic"options={{ max : 25 }}>
                    <Profile alt="profile picture"/>
                </Tilt>
            </Zoom>
          </Col>
          <Col md={6} sm={12}>
            <Fade left={isDesktop} bottom={isMobile} duration={2000} delay={1000} distance="30px">
              <div className="about-wrapper__info">
                <p className="about-wrapper__info-text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime fuga rerum aut commodi debitis laboriosam quos rem provident, tempore officia quaerat illo sit in quae sapiente voluptatibus.
                </p>
                <p className="about-wrapper__info-text">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi, cum dolorem, accusamus.
                </p>
                <p className="about-wrapper__info-text">
                  
                </p>
              </div>
            </Fade>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default About;
